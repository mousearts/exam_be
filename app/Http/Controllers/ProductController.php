<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\UnitRumah;

class ProductController extends Controller
{
    function CreateUnit(Request $request){
        DB::beginTransaction();

        try{


            $kavling = $request -> input('kavling');
            $blok = $request -> input('blok');
            $no_rumah = $request -> input('no_rumah');
            $harga_rumah = $request -> input('harga_rumah');
            $luas_tanah = $request -> input('luas_tanah');
            $luas_bangunan = $request -> input('luas_bangunan');


            $unit = new UnitRumah;
            $unit -> kavling = $kavling;
            $unit -> blok = $blok;
            $unit -> no_rumah = $no_rumah;
            $unit -> harga_rumah = $harga_rumah;
            $unit -> luas_tanah = $luas_tanah;
            $unit -> luas_bangunan = $luas_bangunan;

            $unit->save();


            DB::commit();
        }
        catch(\Exception $e){
            DB::rollback();

            return response()->json(["message" => $e->getMessage()], 500);
        }


    }

    function DeleteUnit($id){
            
            DB::delete('delete from unit_rumahs where id =?', [$id] );

    }
}
